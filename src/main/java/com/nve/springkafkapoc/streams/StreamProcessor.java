package com.nve.springkafkapoc.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface StreamProcessor {
    String REQUESTS = "requests";
    String RESPONSES = "responses";

    @Input(REQUESTS)
    SubscribableChannel requests();

    @Output(RESPONSES)
    MessageChannel responses();

}
