package com.nve.springkafkapoc.service;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.util.MimeTypeUtils;
import com.nve.springkafkapoc.streams.StreamProcessor;

@EnableBinding(StreamProcessor.class)
public class ResponseService {

    private final MessageChannel channel;

    public ResponseService(StreamProcessor processor) {
        this.channel = processor.responses();
    }


    @StreamListener(StreamProcessor.REQUESTS)
    public void sendResponse(String text) {
		this.channel.send(MessageBuilder
				.withPayload(text).setHeader(MessageHeaders.CONTENT_TYPE,
						MimeTypeUtils.TEXT_PLAIN).build());
    }

}
